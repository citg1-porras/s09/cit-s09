package com.zuitt.activity1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class Activity1Application {
	ArrayList<String> enrollees=new ArrayList<>();
	public static void main(String[] args) {
		SpringApplication.run(Activity1Application.class, args);
	}

	@GetMapping("/enroll")
	public String enroll(@RequestParam("user") String user) {
	enrollees.add(user);
	return String.format("Thank you for enrolling, %s!",user);
	}
	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){
		return enrollees;
	}
	@GetMapping("/nameage")
	public String nameage(@RequestParam("name") String name,@RequestParam("age") int age) {

		return String.format("Hello %s!, My age is %d",name ,age);
	}
	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id) {
		String message="";

		if(id.equals("java101")){
			message="Name: Java 101, MWF 8:00am-11:00am, PHP 3000.00";
		}
		else if(id.equals("sql101")){
			message="Name: SQL 101, TTH 1:00pm-4:00pm, PHP 2000.00";
		}
		else if(id.equals("javaee101")){
			message="Name: Java EE 101,MWF 1:00pm-4:00pm, PHP 3500.00";
		}
		else{
			message="Course could not be found";
		}
		return message;

	}

}
